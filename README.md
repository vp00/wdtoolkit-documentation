# WDToolkit

Features
========
* Lua executor, with script saving and a basic in game code editor.
* Noclip. Disabled by default. Activated using TK.SetNoClipAllowed(1).
* Custom API methods for keybinds, delays, drawing text on screen. See [API methods](#api-methods) for more details.
* Script selector overlay - Go to `Menu > Script selector overlay` or press `END` on your keyboard. An overlay will appear in game, which you can use to run saved lua scripts using keyboard hotkeys.
* Teleportation menu - Go to `Online > Teleport` to open it, after which you can pick any MP player in your session and teleport to them.
* Namechanger - Go to `Online > Change Name`. Change your username for online sessions. You can also use the `TK.ChangeName(string name)` lua api method, which is useful if you want to automatically change your name when the game is started.
* Output window. Print has been redirected, and logs will appear in the "lua output" window in the UI.
* Archetype spawner/preset editor. Spawn archetypes, move them around, build stuff, then save them as a preset and share them with the community. Pretty neat.
* A bunch of stuff I've probably forgotten.

Preset editor
=============

There are two kinds of presets. Fixed-point and position dynamic. Fixed-point presets are presets designed for use only at a specific point in the map, meanwhile position dynamic are presets which are designed to be popped down at any point in the map (i.e ramps, huge text etc)

The "default" preset (which is automatically created) is fixed-point. It's not intended to be used to hold any presets for you, it's just there if you ever want to spawn something and not have it be apart of any of your other presets.

## Controls
* `NUMPAD8` - Negative modifier for y position or yaw rotation, depending on the editor mode.
* `NUMPAD2` - Positive modifier for y position or yaw rotation, depending on the editor mode.
* `NUMPAD4` - Positive modifier for x position or pitch rotation, depending on the editor mode.
* `NUMPAD6` - Negative modifier for x position or pitch rotation, depending on the editor mode.
* `NUMPAD7` - Negative modifier for z position or roll rotation, depending on the editor mode.
* `NUMPAD9` - Positive modifier for z position or roll rotation, depending on the editor mode.
* `NUMPAD MINUS` - Switches between rotational and positional editing.
* `NUMPAD DECIMAL` - If a preset is position dynamic, you will be able to modify the base point of the entire preset (which in turn, will move all entities which are apart of the preset). This button will switch to that mode. (and if pressed multiple times will switch between rotational and positional editing).
* `F8` - Empties all spawned entities (unsaved preset changes will be lost)
* `F9` - Spawns a new instance of the last spawned archetype. If the preset is position dynamic, it will spawn at the base point (the position the player was at when pressing "spawn", or the position the player was at when the script was created). If fixed point, the archetype will be spawned at the players current position.
* `NUMPAD DIVIDE` - Deletes the entity in your crosshair, if it's apart of any of your presets.
* `NUMPAD MULTIPLY` - Marks the entity in your crosshair for moving, if it's apart of any of your presets.
* `NUMPAD PLUS` - Makes a copy of the entity (clones position, rotation, archetype) and marks it for moving.

API methods
===========

## TK.RegisterCB(eventName, callback)

Registers a callback for eventName. Possible eventName values are:
* `Draw` - called each frame, refrain from using this (will occasionally cause crashes). Value(s) returned by `callback` are drawn as strings in the same fashion as `TK.SetDrawText()`.
* `KeyDown` - called each time a WM_KEYDOWN wndproc is sent to the game.
* `KeyUp` - called each time a WM_KEYUP wndproc is sent to the game.

## TK.GetKeyState(key)

Wrapper for [GetKeyState](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getkeystate)

### Usage
```lua
if (TK.GetKeyState(50) < 0) then -- virtual key code 50 = P key
  print("Key is down");
else
  print("Key is not down");
end
```
## TK.GetStepRange()

Returns the step range that the preset editor uses (numpad controls)

## TK.SetStepRange()

Sets the step range used by the preset editor (default 0.2)

## TK.SetTimeout(millis, callback)

Waits `millis` milliseconds, then makes a call to `callback`

## TK.ChangeName(name)

Changes the username presented to other players to `name`. Max 15 characters.

Return values:
* `0` - Changed no name, already passed "press key to continue" prompt. Needs to join a lobby (decryption/freeroam) before using this method. After you've been in a lobby, this method will work fine for the rest of your game session.
* `1` - Changed uplay name (before "press any key to continue" prompt)
* `2` - Changed username through games memory

## TK.SetDrawText(text)

Draw `text` in the top right corner of the screen. Unique for all scripts.

## TK.RunFromFile(script)

Runs `script` from the WDToolkit\scripts directory.

## TK.RunFromMemory(script)

Runs `script`, if it's been loaded into memory.

## TK.ExistsInMemory(script)

Checks if `script` is loaded into memory. A script is loaded into memory when it's opened for the first time this game session in the code editor UI.
Returns a boolean representing whether `script` has been loaded into memory.

API methods for uplay version
=============================

## TK.SetLocalPlayerPos(x, y, z)

Sets the position of the local player to specified coordinates.

## TK.SetNoClipAllowed(noclipAllowed)

If `noclipAllowed` is 1, noclip will be possible using the arrow keys. If 0, noclip will be disabled.
Default: `0`

Controls:
* `ArrowUp` -  Increase your y pos
* `ArrowDown` - Decrease your y pos
* `ArrowLeft` - Decrease your x pos
* `ArrowRight` - Increase your x pos
* `ArrowUp + SHIFT` - Increase your z pos
* `ArrowDown + SHIFT` - Decrease your z pos

## TK.SetZPosProtection(zPosProtectionActive)

If `zPosProtection` is 1, whenever the player reaches `z < 5`, they will be teleported back up to a safe distance, to prevent them from getting stuck and having to fast travel. If 0, it will be disabled.
Default: `0`, since it may cause crashes during loading screens.